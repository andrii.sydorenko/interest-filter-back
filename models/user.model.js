const pool = require('../config/db.config')

const getUsers = async (sort_by, order_by, interests) => {
  const results = await pool.query(
    `with groupped_interests(id, interests) as (
      select user_id, array_agg(interest) from users_interests, interests where interests.id = users_interests.interest_id group by user_id
      ), filtered_users as(
      select users.id, interests, first_name, last_name, age, email, registration_date from groupped_interests
      join users on users.id = groupped_interests .id ${
        interests ? `where ARRAY ${interests}:: varchar[] <@ interests` : ''
      }
      )
      select * from filtered_users order by ${sort_by} ${order_by}`
  )
  return results.rows
}

const getUserById = async id => {
  const results = await pool.query(
    `with groupped_interests(id, interests) as (
      select user_id, array_agg(interest) from users_interests, interests where interests.id = users_interests.interest_id AND user_id = $1 group by user_id
      ), filtered_users as(
      select users.id, interests, first_name, last_name, age, email, registration_date from groupped_interests
      join users on users.id = groupped_interests .id
      )
      select * from filtered_users`,
    [id]
  )
  return results.rows
}

module.exports = {
  getUsers,
  getUserById
}
