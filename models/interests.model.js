const pool = require('../config/db.config')

const getInterests = async () => {
  const results = await pool.query('select * from interests;')
  return results.rows
}

module.exports = {
  getInterests
}
