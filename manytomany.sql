create table users (
id INT GENERATED ALWAYS AS IDENTITY,
first_name varchar(255) not null,
last_name varchar(255) not null,
age int not null,
email varchar(255) not null unique,
registration_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
primary key(id)
);

create table interests (
  id int GENERATED ALWAYS AS IDENTITY,
  interest VARCHAR(255) UNIQUE,
  PRIMARY KEY(id)
);

create table users_interests (
  user_id int,
  interest_id int,
  CONSTRAINT fk_user
      FOREIGN KEY(user_id) 
	  REFERENCES users(id),
  CONSTRAINT fk_interest
      FOREIGN KEY(interest_id) 
	  REFERENCES interests(id),
  UNIQUE(user_id, interest_id)
);

insert into users (first_name, last_name, age, email)
VALUES('Andrey', 'Sydorenko', 19, 'andreysydorenko101@gmail.com'),
('Bryony', 'Pate', 27, 'BPate@gmail.com'),
('Hassan', 'Landry', 33, 'hassan@gmail.com');

insert into interests (interest)
values('programming'), ('movies'), ('books'), ('manga'), ('skiing'), ('valleyball'), ('basketball'),
 ('writing'), ('painting'), ('hiking'), ('quizes'), ('hacking');

insert into users_interests (user_id, interest_id) values(1, 1), (1, 2), (1, 3), (1, 4), (1, 6), (1, 11), (1, 12),
(2, 1), (2, 2), (2, 5), (2, 7), (2, 8), (2, 9), (2, 10),
(3, 1), (3, 3), (3, 4), (3, 10), (3, 12), (3, 9), (3, 8);

-- join

with groupped_interests(id, interests) as (
select user_id, array_agg(interest) from users_interests, interests where interests.id = users_interests.interest_id group by user_id
), filtered_users as(
select users.id, interests, first_name, last_name, age, email, registration_date from groupped_interests
join users on users.id = groupped_interests .id where ARRAY ['manga']:: varchar[] <@ interests
)
select * from filtered_users order by id asc