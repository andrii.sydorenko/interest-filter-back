const { Router } = require('express')
const { getUsers, getUserById } = require('../controllers/user.controller')
const { query, param } = require('express-validator')

const userRouter = Router()
const isNumber = /^[0-9]*$/

userRouter.get(
  '/users',
  [
    query('sort_by')
      .optional()
      .matches(
        /\bid\b|\bfirst_name\b|\blast_name\b|\bemail\b|\bage\b|\bregistration_date\b/
      )
      .withMessage(
        'Wrong parameter value you can use: id, first_name, last_name, email, age, registration_date'
      ),
    query('order_by')
      .optional()
      .matches(/\basc\b|\bdesc\b/)
      .withMessage('Wrong parameter value you can use: asc or desc'),
    query('interests')
      .optional()
      .toArray()
      .isArray()
      .withMessage('You have passed not an array')
  ],
  getUsers
)

userRouter.get(
  '/users/:id',
  [
    param('id')
      .matches(isNumber)
      .withMessage('Id must be numeric type, like 1 or 34')
  ],
  getUserById
)

module.exports = userRouter
