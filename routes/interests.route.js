const { Router } = require('express')
const {
  getInterests,
} = require('../controllers/interests.controller')

const interestsRouter = Router()

interestsRouter.get('/interests', getInterests)

module.exports = interestsRouter
