create table users (
user_id INT GENERATED ALWAYS AS IDENTITY,
first_name varchar(255) not null,
last_name varchar(255) not null,
age int not null,
email varchar(255) not null unique,
registration_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
primary key(user_id)
);

create table interests (
  interests_id int GENERATED ALWAYS AS IDENTITY,
  user_id INT,
  interests TEXT [],
  PRIMARY KEY(interests_id),
   CONSTRAINT fk_user
      FOREIGN KEY(user_id) 
	  REFERENCES users(user_id)
);

insert into users (first_name, last_name, age, email)
VALUES('Andrey', 'Sydorenko', 19, 'andreysydorenko101@gmail.com'),
('Bryony', 'Pate', 27, 'BPate@gmail.com'),
('Hassan', 'Landry', 33, 'hassan@gmail.com');

insert into interests (user_id, interests)
values(1, '{"programming", "books", "movies", "books", "manga" }' ),
(3, '{"basketball", "books", "valleyball", "skiing", "movies" }' ),
(2, '{"programming", "books", "writing", "valleyball", "painting" }' );


select * from interests where 'manga'=any(interests);

-- select with interests
select users.user_id, users.first_name, users.last_name, users.age, users.email, users.registration_date, interests.interests
from users
inner join interests on users.user_id=interests.user_id
where 'chess'=any(interests);