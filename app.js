const express = require('express')
const cors = require('cors')
require('dotenv').config()
const userRouter = require('./routes/user.route')
const interestsRouter = require('./routes/interests.route')

const PORT = process.env.PORT

const app = express()

app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())

app.use(userRouter)
app.use(interestsRouter)

app.listen(PORT, () => {
  console.log(`listening to ${PORT} port`)
})
