const { getInterests } = require('../models/interests.model')

exports.getInterests = async (req, res) => {
  const interests = await getInterests()

  res.status(200).json(interests)
}
