const { validationResult } = require('express-validator')
const { getUsers, getUserById } = require('../models/user.model')

exports.getUsers = async (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }

  const { sort_by = 'id', order_by = 'asc', interests } = req.query
  let interestsString;
  if (interests) {
  interestsString = JSON.stringify(interests).replace(/"/g, "'")
  }
  const users = await getUsers(sort_by, order_by, interestsString)

  res.status(200).json(users)
}


exports.getUserById = async (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }
  const id = Number.parseInt(req.params.id)
  console.log(id);

  const users = await getUserById(id)

  res.status(200).json(users)
}