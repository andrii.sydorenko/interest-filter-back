const { Pool } = require('pg')
require('dotenv').config()

const { DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DATABASE } = process.env

const pool = new Pool({
  user: DB_USER,
  host: DB_HOST,
  database: DATABASE,
  password: DB_PASSWORD,
  port: DB_PORT
})

module.exports = pool
